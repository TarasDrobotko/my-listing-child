<?php

namespace MyListing\Src\Listing_Types\Filters;

if (!defined('ABSPATH')) {
	exit;
}

class Range extends Base_Filter
{

	public function filter_props()
	{
		$this->props['type'] = 'range';
		$this->props['label'] = 'Range';
		$this->props['show_field'] = '';
		$this->props['option_type'] = 'range';
		$this->props['step'] = 1;
		$this->props['prefix'] = '';
		$this->props['suffix'] = '';
		$this->props['behavior'] = 'lower';
		$this->props['format_value'] = 1;

		// set allowed fields
		$this->allowed_fields = ['text', 'number'];
	}

	public function apply_to_query($args, $form_data)
	{
		if (empty($form_data[$this->get_form_key()])) {
			return $args;
		}


		$field_key = $this->get_prop('show_field');

		$range_type = $this->get_prop('option_type');
		$behavior = $this->get_prop('behavior');

		$range = $form_data[$this->get_form_key()];

		if (empty($range)) {
			return $args;
		}

		/**
		 * To allow range filters to work with float values, we have to cast values
		 * to DECIMAL in MySQL. The default range is (11,2) which will work with
		 * field values from -999999999.99 to 999999999.99.
		 *
		 * This filter allows to widen the range if needed, for example:
		 * add_filter( 'mylisting/range-filter:typecast', function() {
		 *     return 'DECIMAL(14,2)';
		 * } );
		 *
		 * @since 2.4.5
		 */
		$typecast = apply_filters('mylisting/range-filter:typecast', 'DECIMAL(11,2)', $this);

		if ($range_type === 'range' && strpos($range, '..') !== false) {

			if ($field_key == "prisoner-age") {

				$this->update_posts_meta_val_birthday();

				$range_dates = $this->get_range_dates($range);

				$args['meta_query'][] = [
					'key'     => '_' . $field_key,
					'value'   => $range_dates,
					'compare' => 'BETWEEN',
					'type' => 'DATE',
				];
			} else {
				$args['meta_query'][] = [
					'key'     => '_' . $field_key,
					'value'   => array_map('floatval', explode('..', $range)),
					'compare' => 'BETWEEN',
					'type' => $typecast,
				];
			}
		}

		if ($range_type === 'simple') {
			$args['meta_query'][] = [
				'key' => '_' . $field_key,
				'value' => floatval($range),
				'compare' => $behavior === 'upper' ? '>=' : '<=',
				'type' => $typecast,
			];
		}

		return $args;
	}

	/*      
	* Get array of dates (between which the birthdays fall) 
	* for WP_Meta_Query
	*/
	public function get_range_dates($age_range): array
	{
		$age_range_arr = explode('..', $age_range);
		$range_dates = array();
		foreach ($age_range_arr as $key => $age) {
			$range_dates[$key] = $this->age_to_date($age);
		}

		sort($range_dates);

		$range_dates[0] = strtotime($range_dates[0] . " -1 year +1 day");
		$range_dates[0] = date('Y-m-d', $range_dates[0]);

		return $range_dates;
	}

	public function age_to_date($age)
	{
		list($year, $month, $day) = explode("-", date("Y-m-d"));
		$range = $year - $age;
		$date = strtotime("{$range}-{$month}-{$day}");

		return date('Y-m-d', $date);
	}

	public function get_age_for_post($dateOfBirth)
	{
		$pos = strpos($dateOfBirth, '/');
		if ($pos !== false) {
			$dateOfBirth = date_create_from_format('d/m/Y', $dateOfBirth);
		} else {
			$dateOfBirth = date_create_from_format('Y-m-d', $dateOfBirth);
		}

		$today = date_create(date("Y-m-d"));
		$diff = date_diff($dateOfBirth, $today);
		return $diff->format('%y');
	}

	/*
* Get array of objects with posts data. 
* Each post object contains post id and meta value (date of a birth) properties.
*/
	public function get_posts_data()
	{
		global $wpdb;
		$posts_data = $wpdb->get_results($wpdb->prepare("
				SELECT {$wpdb->posts}.ID, {$wpdb->postmeta}.meta_value
					FROM {$wpdb->posts}
					INNER JOIN {$wpdb->postmeta} ON ( {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id )
					INNER JOIN {$wpdb->postmeta} AS mt1 ON ( {$wpdb->posts}.ID = mt1.post_id )
					WHERE {$wpdb->postmeta}.meta_key = %s
						AND {$wpdb->postmeta}.meta_value != ''
					    AND {$wpdb->posts}.post_type = 'job_listing'
					    AND {$wpdb->posts}.post_status = 'publish'
						AND mt1.meta_key = '_case27_listing_type'
						AND mt1.meta_value = %s
					GROUP BY {$wpdb->posts}.ID
			", '_' . $this->get_prop('show_field'), $this->listing_type->get_slug()));
		return $posts_data;
	}

	/*
* get array of prisoners ages
*/
	public function get_prisoner_ages($formatted_posts_data)
	{
		$prisoner_ages = [];
		foreach ($formatted_posts_data as $post_key => $post) {
			$dateOfBirth = $post->meta_value;
			$prisoner_age = $this->get_age_for_post($dateOfBirth);
			$prisoner_ages[$post_key] = $prisoner_age;
		}

		return $prisoner_ages;
	}

	public function get_formatted_posts_data(): array
	{
		$posts_data = $this->get_posts_data();

		$formatted_posts_data = [];
		foreach ($posts_data as $key => $post) {
			$dateOfBirth = $post->meta_value;
			$formatted_prisoner_birthday = $this->format_prisoner_birthday($dateOfBirth);
			$post->meta_value = $formatted_prisoner_birthday;
			$formatted_posts_data[] = $post;
		}

		return $formatted_posts_data;
	}

	/* 
	* format prisoner birthday to Y-m-d format
	*/
	public function format_prisoner_birthday($prisoner_birthday)
	{
		$pos = strpos($prisoner_birthday, '/');
		if ($pos !== false) {
			list($day, $month, $year) = explode("/", $prisoner_birthday);
			$formatted_prisoner_birthday = "{$year}-{$month}-{$day}";
		} else {
			$formatted_prisoner_birthday = $prisoner_birthday;
		}
		return $formatted_prisoner_birthday;
	}

	/*
	* Update post meta value key
	* for the prisoner with formatted value if needed
	*/
	public function update_posts_meta_val_birthday()
	{
		$formatted_posts_data = $this->get_formatted_posts_data();

		foreach ($formatted_posts_data as $formatted_post_data) {

			$post_id = $formatted_post_data->ID;
			$meta_value = $formatted_post_data->meta_value;
			$meta_key = '_' . $this->get_prop('show_field');
			$old_meta_value = get_post_meta($post_id, $meta_key, true);

			if ($meta_value != $old_meta_value) {
				update_metadata('post', $post_id, $meta_key, $meta_value);
			}
		}
	}

	public function get_range_min()
	{
		if (isset($this->cache['range_min'])) {
			return $this->cache['range_min'];
		}

		global $wpdb;
		$post_id = $wpdb->get_var($wpdb->prepare("
			SELECT {$wpdb->posts}.ID
				FROM {$wpdb->posts}
				INNER JOIN {$wpdb->postmeta} ON ( {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id )
				INNER JOIN {$wpdb->postmeta} AS mt1 ON ( {$wpdb->posts}.ID = mt1.post_id )
				WHERE {$wpdb->postmeta}.meta_key = %s
					AND {$wpdb->postmeta}.meta_value != ''
				    AND {$wpdb->posts}.post_type = 'job_listing'
				    AND {$wpdb->posts}.post_status = 'publish'
					AND mt1.meta_key = '_case27_listing_type'
					AND mt1.meta_value = %s
				GROUP BY {$wpdb->posts}.ID
				ORDER BY {$wpdb->postmeta}.meta_value +0 ASC
				LIMIT 0, 1
		", '_' . $this->get_prop('show_field'), $this->listing_type->get_slug()));

		if (!empty($post_id) && ($min_value = get_post_meta($post_id, '_' . $this->get_prop('show_field'), true))) {
			$f_key = $this->get_prop('show_field');

			if ($f_key == 'prisoner-age') {
				$formatted_posts_data = $this->get_formatted_posts_data();

				if (!empty($formatted_posts_data)) {
					$prisoner_ages = $this->get_prisoner_ages($formatted_posts_data);

					$min_age = min($prisoner_ages);
					$age_for_range = $min_age;

					$this->cache['range_min'] = (float) $age_for_range;
				}
			} else {
				$this->cache['range_min'] = (float) $min_value;
			}
		} else {
			$this->cache['range_min'] = 0;
		}

		return $this->cache['range_min'];
	}

	public function get_range_max()
	{
		if (isset($this->cache['range_max'])) {
			return $this->cache['range_max'];
		}

		global $wpdb;
		$post_id = $wpdb->get_var($wpdb->prepare("
			SELECT {$wpdb->posts}.ID
				FROM {$wpdb->posts}
				INNER JOIN {$wpdb->postmeta} ON ( {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id )
				INNER JOIN {$wpdb->postmeta} AS mt1 ON ( {$wpdb->posts}.ID = mt1.post_id )
				WHERE {$wpdb->postmeta}.meta_key = %s
					AND {$wpdb->postmeta}.meta_value != ''
				    AND {$wpdb->posts}.post_type = 'job_listing'
				    AND {$wpdb->posts}.post_status = 'publish'
					AND mt1.meta_key = '_case27_listing_type'
					AND mt1.meta_value = %s
				GROUP BY {$wpdb->posts}.ID
				ORDER BY {$wpdb->postmeta}.meta_value +0 DESC
				LIMIT 0, 3
		", '_' . $this->get_prop('show_field'), $this->listing_type->get_slug()));

		if (!empty($post_id) && ($max_value = get_post_meta($post_id, '_' . $this->get_prop('show_field'), true))) {

			$f_key = $this->get_prop('show_field');

			if ($f_key == 'prisoner-age') {
				$formatted_posts_data = $this->get_formatted_posts_data();

				if (!empty($formatted_posts_data)) {
					$prisoner_ages = $this->get_prisoner_ages($formatted_posts_data);

					$max_age = max($prisoner_ages);
					$age_for_range = $max_age;

					$this->cache['range_max'] = (float) $age_for_range;
				}
			} else {
				$this->cache['range_max'] = (float) $max_value;
			}
		} else {
			$this->cache['range_max'] = 0;
		}

		return $this->cache['range_max'];
	}

	public function get_request_value()
	{
		return !empty($_GET[$this->get_form_key()])
			? sanitize_text_field($_GET[$this->get_form_key()])
			: '';
	}
}
