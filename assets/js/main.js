jQuery(function ($) {
  $(".job_listing-template-default.type-voprosy-2 .show-review-form").click(
    function () {
      setTimeout(function () {
        $('.sidebar-comment-form textarea[name="comment"]').focus();
      }, 250);
    }
  );

  $(".type-voprosy-2 .sidebar-comment-form .buttons.button-2").text(
    "Обновить ответ"
  );

  $(".type-voprosy-2 #commentform .buttons.button-2").text(
    "Опубликовать ответ"
  );

  $(".type-voprosy-2 .tab-type-comments .pf-head h5").text("Добавить ответ");

  $("a.user-profile-name").hover(function () {
    $("a.user-profile-name .submenu-toggle").toggleClass("highlight");
  });

  /* detect flex-wrap */
  wrapped();

  $(window).resize(function () {
    wrapped();
  });

  function wrapped() {
    var offset_top_prev;

    $(".header-links-container ul li").each(function () {
      var offset_top = $(this).offset().top;

      if (offset_top > offset_top_prev) {
        $(this).prev().addClass("wrapped");
      } else if (offset_top == offset_top_prev) {
        $(this).prev().removeClass("wrapped");
      }

      offset_top_prev = offset_top;
    });
  }

  /*
   * delele dublicate tabs on explorer page in sidebar
   */
  $(window).on("load", function () {
    let ulList = $(
        "#finderSearch .finder-tabs.with-listing-types ul.nav.nav-tabs.tabs-menu"
      ),
      tabContentList = $(
        "#finderSearch .finder-tabs.with-listing-types div.tab-content"
      ),
      listForRemove = [],
      listVisible = [],
      tabListForRemove = [];
    tabListVisible = [];
    $(ulList).each(function () {
      if ($(this).css("display") == "none") {
      } else {
        listVisible.push($(this));
      }
    });

    $(listVisible).each(function (index) {
      if (index != 0) {
        $(this).remove();
      }
    });

    $(tabContentList).each(function () {
      if ($(this).css("display") == "none") {
      } else {
        tabListVisible.push($(this));
      }
    });

    $(tabListVisible).each(function (index) {
      if (index != 0) {
        $(this).remove();
      }
    });
  });

  $(".explore-types.cts-carousel .item").on("click", () => {
    let ulList = $(
        "#finderSearch .finder-tabs.with-listing-types ul.nav.nav-tabs.tabs-menu"
      ),
      tabContentList = $(
        "#finderSearch .finder-tabs.with-listing-types div.tab-content"
      ),
      listForRemove = [],
      listVisible = [],
      tabListForRemove = [];
    tabListVisible = [];
    $(ulList).each(function () {
      if ($(this).css("display") == "none") {
      } else {
        listVisible.push($(this));
      }
    });

    $(listVisible).each(function (index) {
      if (index != 0) {
        $(this).remove();
      }
    });

    $(tabContentList).each(function () {
      if ($(this).css("display") == "none") {
      } else {
        tabListVisible.push($(this));
      }
    });
    $(tabListVisible).each(function (index) {
      if (index != 0) {
        $(this).remove();
      }
    });
  });

  $(document).ready(function () {
    /*
     * Change image gallery sizes
     */
    setTimeout(function () {
      changeImgHeight();
    }, 200);

    function changeImgHeight() {
      if ($(".single .owl-item.active a")[0]) {
        let width = document.querySelector(".owl-item.active").style.width;
        let height = parseInt(width, 10);
        let num = document.querySelectorAll(".owl-item.active").length;
        if (num == 2) {
          height = parseInt(width, 10) - 180;
        }
        $(".owl-item.active .item").height(height);
      } else {
        setTimeout(function () {
          changeImgHeight();
        }, 200);
      }
    }

    /*  hover effect for section with white and black squares on the main page */
    $(".listing-types-block > a").hover(
      function (e) {
        $(this).find(".listing-type-item-hover").css("display", "flex");
        $(this).find(".listing-type-item").css("display", "none");

        if ($(e.target).is(".listing-types-block > a:nth-child(1)")) {
        }
      },
      function () {
        $(this).find(".listing-type-item-hover").css("display", "none");
        $(this).find(".listing-type-item").css("display", "flex");
      }
    );

    /*
     * remove icon for fields with taxonomies on listing page
     */
    if (!$(".block-type-terms .cat-icon i").hasClass("") === false) {
      $(".block-type-terms .cat-icon").css("display", "none");
      $(
        "#c27-single-listing .block-type-terms .listing-details ul li span.category-name"
      ).attr("style", "font-weight: 400 !important");
    } else {
      $(".block-type-terms .cat-icon").css("display", "inline");
    }

    /*
     * input mask for the Age field with text type
     * and date validation
     */

    var input = document.getElementById("prisoner-age");
    if (typeof input != "undefined" && input != null) {
      var dateInputMask = function dateInputMask(elm) {
        elm.addEventListener("keypress", function (e) {
          if (e.keyCode < 47 || e.keyCode > 57) {
            e.preventDefault();
          }

          var len = elm.value.length;

          // If we're at a particular place, let the user type the slash
          // i.e., 12/12/1212
          if (len !== 1 || len !== 3) {
            if (e.keyCode == 47) {
              e.preventDefault();
            }
          }

          // If they don't add the slash, do it for them...
          if (len === 2) {
            elm.value += "/";
          }

          // If they don't add the slash, do it for them...
          if (len === 5) {
            elm.value += "/";
          }
        });
      };

      dateInputMask(input);
    }
  });

  /*
   * appeppend title to question block
   */
  if (!$(".answer-title")[0]) {
    $("#question-block")
      .parent()
      .append(
        '<div class="col-md-12 block-type-text block-field-job_title"><div class="title-style-1 answer-title"><h5>Ответы</h5></div></div>'
      );
  }

  /*
   * border on add listing page
   */
  let backgroundColor = $(".ac-back-side.face").css("background-color");
  $(".face.ac-front-side").css("border-color", backgroundColor);

  /*
   * change font-size on add listing page
   */
  let fontSizeFront = $(".ac-front-side.face .category-name").css("font-size");
  $(".ac-back-side.face p").css("font-size", fontSizeFront);

  /*
   * scroll into view on explore page
   */

  window.onload = function () {
    window.onunload = setTimeout(function () {
      if ($(".explore-head .item.active")[0]) {
        let element = $(".explore-head .item.active");
        scrollTo = element.position().left;
        $scroller = $(".explore-types.cts-carousel");
        $scroller.scrollLeft(scrollTo);
      }
    }, 200);
  };

  /*
   * append button (Back to) on single listing page
   */
  if (!$(".back-to-explore-btn")[0] && $("body").hasClass("single-listing")) {
    let base_url = window.location.origin,
      bodyClassList = document.querySelector("body.single-listing").classList,
      substring = "type";

    bodyClassList.forEach(function (entry) {
      if (entry.includes(substring)) {
        singleListingType = entry.split("-")[1];
      }
    });

    let explorePage = base_url + "/explore/?type=" + singleListingType;
    $("a.listing-tab-toggle:last")
      .closest("li")
      .after(
        '<li class="back-to-explore-li"><a href="' +
          explorePage +
          '" class="back-to-explore-btn">На страницу поиска</a></li>'
      );

    if (window.matchMedia("(max-width: 600px)").matches) {
      $(".single-listing .profile-header").after(
        '<div class="back-to-explore-small"><a href="' +
          explorePage +
          '" class="back-to-explore-btn">На страницу поиска</a></div>'
      );
    }
  }
});
