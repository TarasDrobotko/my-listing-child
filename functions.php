<?php

// Enqueue child theme style.css
add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri() );

     wp_enqueue_script( 'main-js', get_stylesheet_directory_uri(). '/assets/js/main.js', array('jquery'),
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
    
     wp_enqueue_script( 'parsley-ru', get_stylesheet_directory_uri(). '/assets/js/parsley-ru.js', 'parsley'
    );

    if ( is_rtl() ) {
    	wp_enqueue_style( 'mylisting-rtl', get_template_directory_uri() . '/rtl.css', [], wp_get_theme()->get('Version') );
    }
}, 500 );


/*
 * remove admin bar for role author_expert
 */
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
if (current_user_can('author_expert')) {
show_admin_bar(false);
}
}

/* 
load child theme texdomain 
for loading files which create with Loco translate plugin
*/
function my_listing_child_setup()
{
    $path = get_stylesheet_directory() . '/languages/loco/themes/';
    load_child_theme_textdomain('my-listing-child', $path);
}
add_action('after_setup_theme', 'my_listing_child_setup');

/*
 * close comments
*/
add_action( 'wp_head', 'migr_comments_open');

function migr_comments_open() {
	
	$custom = get_post_custom();
	// print_r($custom);

$listing_type = get_post_meta(get_the_ID(), '_case27_listing_type', TRUE);
 	if ( 'voprosy-2' == $listing_type && !current_user_can('administrator') && !current_user_can('author_expert')) {
 	    
 	    ?>
 
     <script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.reply.comment-info, .show-review-form, .comments-list-wrapper+div').css("display", "none");
      }); 
        </script>
   <?php     
}
}

/*
 *  enable acf
*/
add_filter( 'acf/settings/show_admin', '__return_true', 50 );

/*
 * remove name category on title
*/
add_filter( 'get_the_archive_title', 'artabr_remove_name_cat' );
function artabr_remove_name_cat( $title ){
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	}
	return $title;
}
/* 
 * check user role
 */
add_action('wp_footer', 'get_user_role');
function get_user_role() {
    global $current_user;

    $user_roles = $current_user->roles;
    $user_role = array_shift($user_roles);
    //print_r($user_role);
   
    return $user_role;
}

/* 
 * check which template is loaded 
*/
function migr_which_template_is_loaded()
{
	if (is_super_admin()) {
		//global $template;
	//	print_r($template);
	}
}

add_action('wp_footer', 'migr_which_template_is_loaded');

// function my_new_customer_data($new_customer_data){
//  $new_customer_data['role'] = 'author';
//  return $new_customer_data;
// }
// add_filter( 'woocommerce_new_customer_data', 'my_new_customer_data');

/*
* dissable plugin updates for Mailoptin plugin/
*/
add_filter('site_transient_update_plugins', 'remove_update_notification');
function remove_update_notification($value) {
     unset($value->response["mailoptin/mailoptin.php"]);
     return $value;
} 


// add_action( 'init', 'create_tag_taxonomies', 0 );

// function create_tag_taxonomies() 
// {
//   // Add new taxonomy, NOT hierarchical (like tags)
//   $labels = array(
//     'name' => _x( 'Tags', 'taxonomy general name' ),
//     'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
//     'search_items' =>  __( 'Search Tags' ),
//     'popular_items' => __( 'Popular Tags' ),
//     'all_items' => __( 'All Tags' ),
//     'parent_item' => null,
//     'parent_item_colon' => null,
//     'edit_item' => __( 'Edit Tag' ), 
//     'update_item' => __( 'Update Tag' ),
//     'add_new_item' => __( 'Add New Tag' ),
//     'new_item_name' => __( 'New Tag Name' ),
//     'separate_items_with_commas' => __( 'Separate tags with commas' ),
//     'add_or_remove_items' => __( 'Add or remove tags' ),
//     'choose_from_most_used' => __( 'Choose from the most used tags' ),
//     'menu_name' => __( 'Tags' ),
//   ); 

//   register_taxonomy('post_tag',array('job_listing', 'page'),array(
//     'hierarchical' => false,
//     'labels' => $labels,
//     'show_ui' => true,
//     'update_count_callback' => '_update_post_term_count',
//     'query_var' => true,
//     'rewrite' => array( 'slug' => 'post_tag' ),
//     //'_builtin' => true
//   ));
  
 
//  }


/* listing types list block */
add_shortcode( 'listing-types', 'listing_types_func' );

function listing_types_func( $atts ){
	$args = array(
              'post_type' => 'case27_listing_type',
              'orderby'   => 'menu_order',
              'post_status' => 'publish',
              'posts_per_page' => -1,
            );
$listing_types = new WP_Query;
$listing_types = $listing_types->query($args);
	 echo '<div class="listing-types-block">';
	 
	 	 foreach($listing_types as $listing_type) { 
	 	 /*  Check if listing type is Russia or Belarus */
	 	if($listing_type->ID == 111 || $listing_type->ID == 6932) {
	 	 ?>
<a href="<?php echo get_site_url(). '/explore/?type='. $listing_type->post_name; ?>"> <div class="listing-type-item"><span> <?php echo esc_html( $listing_type->post_title); ?> </span></div>
<div class="listing-type-item-hover"><span>
    <?php  $square_hover_text = c27()->get_setting( 'tekst_pri_navedenii_kursora_na_kvadrat' ); 
    echo $square_hover_text;
    ?></span></div>
</a>
<?php
}
 }?>
 <div class="ampersand"><img src="/wp-content/uploads/2023/09/ampersand-2-2.webp"></div>
 <?php 
 
 echo '</div>';
}

// Shortcode to output custom PHP in Elementor
function elementor_add_btn_shortcode( $atts ) {
    $label = c27()->get_setting( 'main_screen_call_to_action_label' );
    $links_to = c27()->get_setting('main_screen_call_to_action_links_to' );

if ( is_user_logged_in() && ! \MyListing\Src\User_Roles\user_can_add_listings() ) {
	return;
}

// if ( ! ( $data['show_call_to_action'] && $label && $links_to ) ) {
// 	return;
// }

?>
<?php 
echo '<div class="main_screen_button_block"><span>Размещайте объявления!</span>';
echo '<div class="header-button main_screen_button">';
echo '<a href="'. esc_url( $links_to ) .'" class="buttons button-2">'; 
echo do_shortcode( $label ); 
echo 	'</a></div>';
echo '</div>';
}
add_shortcode( 'main_screen_btn_output', 'elementor_add_btn_shortcode');


/**
 * Display select in admin (filter by Listing Type)
 */
 add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'job_listing'; // change to your post type

	$values = \MyListing\get_posts_dropdown( 'case27_listing_type' );
	
	  if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }

    //only add filter to post type you want
    if ($post_type == $type){
	 ?>
        <select name="ADMIN_FILTER_FIELD_NAME">
            <option value="">Фильтр по Listing Type</option>
            <?php
             $current = isset($_GET['ADMIN_FILTER_FIELD_NAME'])? $_GET['ADMIN_FILTER_FIELD_NAME']:'';
            
            foreach ($values as $value=> $label ) {
                 printf
                 (
                    '<option value="%s"%s>%s</option>',
                    $value,
                     $value == $current? ' selected="selected"':'',
                     $label
                 );
            }
             ?>
        </select>
  <?php
    }
    }
 /**
  * Filter Listing Types by Listing Type in admin. Remove &action2=-1, &action=-1
  */
 add_filter('pre_get_posts', 'tsm_convert_id_to_term_in_query');
 function tsm_convert_id_to_term_in_query($query) {
 	global $pagenow;
 	$post_type = 'job_listing'; // change to your post type
	
 	  $values = \MyListing\get_posts_dropdown( 'case27_listing_type');
	  
 	  $values_with_id = \MyListing\get_posts_dropdown( 'case27_listing_type', 'post_name' );
   
  if ($post_type == $_GET['post_type'] && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_NAME']) && $_GET['ADMIN_FILTER_FIELD_NAME'] != '') {
      
      foreach($values_with_id as $key => $value) {
          if($value == $values[$_GET['ADMIN_FILTER_FIELD_NAME']]) {
              $listing_slug = $key;
          }
      }
          $reguest_uri = $_SERVER[REQUEST_URI]; 
          $uri1 = add_query_arg( 'filter_by_type', $listing_slug); 
        //  $uri2 = str_replace('&action2=-1', '', $uri1);
        //  $uri2 = str_replace('&action=-1', '', $uri2);
             if(isset($_GET["action"]) && $_GET["action"] == -1 || isset($_GET["action2"]) && $_GET["action2"] == -1) {
                  $uri2 = remove_query_arg( 
	array('action', 'action2'), $uri1 );
                 wp_safe_redirect($uri2);
             }
     }
     
     elseif(is_admin()) {
  //  Remove &action2=-1, &action=-1 in admin filters
    global $wp;
      $uri1 = add_query_arg( $wp->query_vars, home_url( $wp->request ) );
        if(isset($_GET["action"]) && $_GET["action"] == -1 || isset($_GET["action2"]) && $_GET["action2"] == -1) {
                  $uri2 = remove_query_arg( 
	array('action', 'action2'), $uri1 );
                wp_safe_redirect($uri2);
             }
             
      global $pagenow;
     if($pagenow == 'upload.php' && $_GET['post_type'] == 'attachment') {
          $mime_type =  $_GET["post_mime_type"];
          ?>
           <script>
        
document.addEventListener("DOMContentLoaded",function() {
    var select = document.getElementById("attachment-filter");
               var value = select.options[select.selectedIndex].value;
               value = 'post_mime_type:'+'<?=$mime_type?>';
select.value = value;
});
                 </script>
                 <?php
     } 
     }
 }
 
 

 ?>